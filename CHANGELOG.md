# Changelog



## [v1.6.0-SNAPSHOT] - 2022-05-20

### Features

- Updated to StorageHub 2



## [v1.5.3] - 2020-05-20

### Fixes

- Updated library org.eclipse.egit.github.core to open-jdk8 and TLSv1.2 [#18318]



## [v1.5.0] - 2018-10-01

### Features

- Updated to StorageHub [11725]



## [v1.4.0] - 2017-06-12

### Features

- Support Java 8 compatibility [#8541]



## [v1.3.0] - 2017-02-15

### Features

- Updated PortalContext support [#6279]



## [v1.2.0] - 2016-12-15

### Features

- Added Portal Context



## [v1.1.0] - 2016-10-01

### Features

- Removed accesslogger dependency
- Updated to Auth 2.0



## [v1.0.0] - 2016-07-01

- First release



This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).